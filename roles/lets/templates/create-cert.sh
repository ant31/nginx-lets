#!/bin/bash

# automation of certificate renewal for let's encrypt and haproxy
# - checks all certificates under /etc/letsencrypt/live and renews
#   those about about to expire in less than 4 weeks
# - creates haproxy.pem files in /etc/letsencrypt/live/domain.tld/
# - soft-restarts haproxy to apply new certificates
# usage:
# sudo ./cert-renewal-haproxy.sh

###################
## configuration ##
###################

EMAIL="2t.antoine@gmail.com"

LE_CLIENT="/opt/letsencrypt/letsencrypt-auto"

HAPROXY_RELOAD_CMD="service haproxy reload"

WEBROOT="/var/lib/haproxy"
DOMAIN=$1
# Enable to redirect output to logfile (for silent cron jobs)
# LOGFILE="/var/log/certrenewal.log"

######################
## utility function ##
######################

function issueCert {
    $LE_CLIENT certonly --text --webroot --webroot-path ${WEBROOT} -d ${DOMAIN}  --renew-by-default --agree-tos --email ${EMAIL}
    cat /etc/letsencrypt/live/${DOMAIN}/privkey.pem /etc/letsencrypt/live/${DOMAIN}/fullchain.pem | tee /etc/letsencrypt/live/${DOMAIN}/haproxy.pem >/dev/null
    ln -s /etc/letsencrypt/live/${DOMAIN}/haproxy.pem /etc/haproxy/certs/${DOMAIN}.pem
  return $?
}

issueCert
